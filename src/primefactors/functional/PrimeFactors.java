package primefactors.functional;

import fj.data.List;

import static fj.data.List.list;

public class PrimeFactors {
    private final Integer number;

    public PrimeFactors(Integer number) {
        this.number = number;
    }

    public List<Integer> primeFactors() {
        return primeFactors(number, 2, List.<Integer>nil());
    }

    private List<Integer> primeFactors(Integer number, Integer factor, List<Integer> list) {
        if (number <= 1)
            return list;

        if (number % factor == 0)
            return primeFactors(number / factor, factor, list(factor).append(list));

        return primeFactors(number, factor + 1, list);
    }
}
