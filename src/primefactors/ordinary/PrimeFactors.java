package primefactors.ordinary;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactors {
    private int number;

    public PrimeFactors(int number) {
        this.number = number;
    }

    public List<Integer> primeFactors() {
        List<Integer> factors = new ArrayList<Integer>();
        for (int i = 2; i <= number; i++) {
            while (number % i == 0) {
                factors.add(i);
                number /= i;
            }
        }
        return factors;
    }
}
