package primefactors.multithreaded;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactors {
    public static final int MAX_THREADS = 10;

    public List<Integer> primeFactors(int number) {

        List<Thread> threadList = new ArrayList<Thread>();
        List<Integer> factors = new ArrayList<Integer>();
        List<int[]> threadData = getThreadMinMax(number);

        for (int[] minMax : threadData) {
            Thread thread = new Thread(new FactorProcessor(factors, number, minMax[0], minMax[1]));
            thread.start();
            threadList.add(thread);
        }

        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return factors;
    }

    public List<int[]> getThreadMinMax(int number) {
        double max = Math.ceil(Math.sqrt(number));
        double possible = max - 2;
        int step = (int) Math.ceil(possible / MAX_THREADS);
        if (step == 0)
            step = 1;

        List<int[]> minMax = new ArrayList<int[]>();
        for (int i = 2; i <= max; i += step) {
            minMax.add(new int[]{i, i + step - 1});
        }
        return minMax;
    }
}

class FactorProcessor implements Runnable {
    private static final Object lock = new Object();

    private List<Integer> factors;
    private int number;
    private int minDivisor;
    private int maxDivisor;

    FactorProcessor(List<Integer> factors, int number, int minDivisor, int maxDivisor) {
        this.factors = factors;
        this.number = number;
        this.minDivisor = minDivisor;
        this.maxDivisor = maxDivisor;
    }

    public void run() {
        for (int i = minDivisor; i <= maxDivisor; i++) {
            if (!isDivisorPrime(i))
                continue;

            while (number % i == 0) {
                synchronized (lock) {
                    factors.add(i);
                }
                number /= i;
            }
        }
    }

    private boolean isDivisorPrime(int i) {
        boolean divisorPrime = true;
        for (int j = 2; j <= i; j++) {
            if (i % j == 0 && i != j) {
                divisorPrime = false;
                break;
            }
        }
        return divisorPrime;
    }
}
