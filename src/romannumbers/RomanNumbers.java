package romannumbers;

/**
 * User: Adipa
 * Date: 12/16/12
 * Time: 8:02 PM
 */
public class RomanNumbers {
    private static int[] denominators = new int[]{1000, 500, 100, 50, 10, 5, 1};
    private static String[] symbols = new String[]{"M", "D", "C", "L", "X", "V", "I"};

    public String toRoman(int i) {
        StringBuilder roman = new StringBuilder();
        createForNumber(i, roman);
        return roman.toString();
    }

    private void createForNumber(int i, StringBuilder sb) {
        if (i == 0)
            return;

        int remaining = processExactMatch(i, sb);

        for (int denominator : denominators) {
            if (remaining > denominator) {
                remaining -= denominator;
                sb.append(findSymbol(denominator));
                break;
            } else {
                for (int innerDenominator : denominators) {
                    if (denominator != innerDenominator
                            && denominator - innerDenominator == remaining) {
                        remaining = 0;
                        sb.append(findSymbol(innerDenominator));
                        sb.append(findSymbol(denominator));
                        break;
                    }
                }
            }
        }
        createForNumber(remaining, sb);
    }

    private int processExactMatch(int i, StringBuilder sb) {
        int remaining = i;
        for (int denominator : denominators) {
            if (denominator == i) {
                remaining -= denominator;
                sb.append(findSymbol(denominator));
            }
        }
        return remaining;
    }

    private String findSymbol(int denominator) {
        for (int i = 0, denominatorsLength = denominators.length; i < denominatorsLength; i++) {
            int value = denominators[i];
            if (value == denominator)
                return symbols[i];
        }
        return "";
    }
}
