package numbernames;

/**
 * User: adipa
 * Date: 4/14/13
 * Time: 9:11 AM
 * Source code for dojo
 */
public class NumberNames {
    public static String[] NUMBERS_LESS_THAN_TWENTY = new String[]{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"
            , "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};

    public static String[] MULTIPLES_OF_TEN = new String[]{"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

    private int number;

    public NumberNames(int number) {
        this.number = number;
    }

    public String toName() {
        String value = "";

        value += processDenominator(1000, "thousand", ",");
        value += processDenominator(100, "hundred", " and");
        value += processMultipliersOfTen();
        if (number > 0)
            value += NUMBERS_LESS_THAN_TWENTY[number - 1];
        return value.trim();
    }

    private String processDenominator(int denominator, String denominatorName, String suffix) {
        String value = "";
        if (number >= denominator) {
            int prefix = number / denominator;
            number = number - prefix * denominator;
            value = new NumberNames(prefix).toName() + " " + denominatorName;
            if (number > 0) {
                value += suffix + " ";
            }
        }
        return value;
    }

    private String processMultipliersOfTen() {
        for (int denominator = 90; denominator >= 20; denominator -= 10) {
            if (number >= denominator) {
                number -= denominator;
                return MULTIPLES_OF_TEN[denominator / 10 - 2] + " ";
            }
        }
        return "";
    }
}
