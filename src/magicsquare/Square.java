package magicsquare;

import fj.F;
import fj.F2;
import fj.Ord;
import fj.data.List;

/**
 * User: adipa
 * Date: 6/1/13
 * Time: 6:21 PM
 * Source code for dojo
 */
public class Square {
    final List<Cell> cells;
    final int order;

    public Square(List<Cell> cells, int order) {
        this.cells = cells;
        this.order = order;
    }

    public boolean isCandidateMagicSquare() {
        Integer sum = findSumIfExists();
        return sum == null || !hasMismatchingSum(sum);
    }

    public boolean isMagicSquare() {
        return isCandidateMagicSquare() && isCompleted();
    }

    private Integer findSumIfExists() {
        Integer sum;
        for (int i = 0; i < order; i++) {
            sum = sumOfRow(i);
            if (sum != null) return sum;

            sum = sumOfColumn(i);
            if (sum != null) return sum;
        }

        sum = sumOfDiagonal(true);
        if (sum != null) return sum;

        sum = sumOfDiagonal(false);
        return sum;
    }

    private boolean hasMismatchingSum(Integer firstSum) {
        Integer sum;
        for (int i = 0; i < order; i++) {
            sum = sumOfRow(i);
            if (sum != null && !sum.equals(firstSum)) return true;

            sum = sumOfColumn(i);
            if (sum != null && !sum.equals(firstSum)) return true;
        }

        sum = sumOfDiagonal(true);
        if (sum != null && !sum.equals(firstSum)) return true;

        sum = sumOfDiagonal(false);
        return (sum != null && !sum.equals(firstSum));
    }

    private boolean isCompleted() {
        return cells.forall(new F<Cell, Boolean>() {
            @Override
            public Boolean f(Cell cell) {
                return cell.value > 0;
            }
        }) && cells.length() == order * order;
    }

    private Integer sumOfRow(final int row) {
        return FilterAndSum(new F<Cell, Boolean>() {
            @Override
            public Boolean f(Cell cell) {
                return cell.y == row;
            }
        });
    }

    private Integer sumOfColumn(final int col) {
        return FilterAndSum(new F<Cell, Boolean>() {
            @Override
            public Boolean f(Cell cell) {
                return cell.x == col;
            }
        });
    }

    private Integer sumOfDiagonal(final boolean left) {
        return FilterAndSum(new F<Cell, Boolean>() {
            @Override
            public Boolean f(Cell cell) {
                if (left)
                    return cell.x == cell.y;
                else
                    return (cell.x + cell.y) == order - 1;
            }
        });
    }

    private Integer FilterAndSum(F<Cell, Boolean> filter) {
        List<Cell> filtered = cells.filter(filter);

        if (filtered.length() != order)
            return null;

        return filtered.foldLeft(new F2<Integer, Cell, Integer>() {
            @Override
            public Integer f(Integer seed, Cell cell) {
                return seed + cell.value;
            }
        }, 0);
    }

    public void print() {
        System.out.println("__________________________________________________");
        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < order; x++) {
            sb.append("|");
            for (int y = 0; y < order; y++) {
                Cell cell = findCell(x, y);
                sb.append(cell.value).append("|");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
        System.out.println("__________________________________________________");
    }

    public List<Square> findNextSet() {
        if (isMagicSquare() || !isCandidateMagicSquare()) return List.nil();
        final Integer next = findNextValue();

        return List.range(0,order).bind(List.range(0, order), new F2<Integer, Integer, Square>() {
            @Override
            public Square f(Integer x, Integer y) {
                if (findCell(x, y) == null) {
                    return new Square(List.list(new Cell(x,y,next)).append(cells),order);
                }
                return null;
            }
        }).filter(new F<Square, Boolean>() {
            @Override
            public Boolean f(Square square) {
                return square != null;
            }
        });
    }

    private Cell findCell(final int x, final int y) {
        List<Cell> filtered = cells.filter(new F<Cell, Boolean>() {
            @Override
            public Boolean f(Cell cell) {
                return cell.x == x && cell.y == y;
            }
        });
        return filtered.isEmpty() ? null : filtered.head();
    }

    private Integer findNextValue() {
        final List<Integer> used = cells.map(new F<Cell, Integer>() {
            @Override
            public Integer f(Cell cell) {
                return cell.value;
            }
        });

        return used.maximum(Ord.<Integer>comparableOrd()) + 1;
    }
}

class Cell {
    final int x;
    final int y;
    final int value;

    Cell(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }
}

