package magicsquare;

import fj.F;
import fj.F2;
import fj.P;
import fj.data.List;

/**
 * User: adipa
 * Date: 6/1/13
 * Time: 7:55 PM
 * Source code for dojo
 */
public class SquareBuilderThread extends Thread {
    final Square initial;
    final F<Square, Void> callback;

    public SquareBuilderThread(Square initial, F<Square, Void> callback) {
        this.initial = initial;
        this.callback = callback;
    }

    @Override
    public void run() {
        findAndValidateNext(initial);
        //findAndValidateNext2(initial);
    }

    private void findAndValidateNext(Square current) {
        if (current.isMagicSquare()) {
            callback.f(current);
        } else {
            List<Square> nextSet = current.findNextSet();
            for (Square next : nextSet) {
                findAndValidateNext(next);
            }
        }
    }

    private void findAndValidateNext2(Square current) {
        if (current.isMagicSquare()) {
            callback.f(current);
        } else {
            List<Square> nextSet = current.findNextSet();
            for (Square next : nextSet) {
                SquareBuilderThread thread = new SquareBuilderThread(next,callback);
                try {
                    thread.start();
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args){
        List.range(0,3).bind(List.range(0, 3), new F2<Integer, Integer, Void>() {
            @Override
            public Void f(Integer x, Integer y) {
                Square square = new Square(List.list(new Cell(x, y, 1)), 3);
                Thread t = new SquareBuilderThread(square, new F<Square, Void>() {
                    @Override
                    public Void f(Square square) {
                        square.print();
                        return null;
                    }
                });
                try {
                    t.start();
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
    }
}
