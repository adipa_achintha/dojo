package printdiamond;

import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: Adipa
 * Date: 6/3/12
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class Diamond1 {
    public static char[][] createDiamond(char c) {
        int index = (c - 'A');
        int size = index * 2 + 1;
        char[][] retAr = new char[size][size];

        for (int i = 0; i < size; i++) {
            Arrays.fill(retAr[i], ' ');
        }

        for (int i = 0; i <= index; i++) {
            char curChr = (char) ('A' + i);
            int x = i;
            int y = index - i;
            int l = size - 1 - 2 * x;
            int w = size - 1 - 2 * y;

            retAr[x][y] = curChr;
            retAr[x][y + w] = curChr;
            retAr[x + l][y] = curChr;
            retAr[x + l][y + w] = curChr;
        }
        return retAr;
    }
}
