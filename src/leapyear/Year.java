package leapyear;

/**
 * User: adipa
 * Date: 3/9/13
 * Time: 12:33 PM
 * Source code for functionaljava
 */
public class Year {
    private final int year;

    public Year(int year) {
        this.year = year;
    }

    public boolean isLeapYear() {
        return (year % 400 == 0 || !(year % 100 == 0)) && year % 4 == 0;
    }
}
