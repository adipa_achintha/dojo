package immutabletest;

import org.junit.Test;

/**
 * User: adipa
 * Date: 5/24/13
 * Time: 8:33 PM
 * Source code for functionaljava
 */
public class MutableTest {
    @Test
    public void testMutable() throws InterruptedException {
        MutableDoor door = new MutableDoor();
        Thread threadA = new Thread(new MutableDoorThread(door, 300));
        Thread threadB = new Thread(new MutableDoorThread(door, 500));
        threadA.start();
        threadB.start();

        threadA.join();
        threadB.join();
    }
}

class MutableDoor {
    private boolean open;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }
}

class MutableDoorThread implements Runnable {
    MutableDoor door;
    int delay;

    MutableDoorThread(MutableDoor door, int delay) {
        this.door = door;
        this.delay = delay;
    }

    public void run() {
        int count = 10;
        while (count > 0) {
            try {
                door.setOpen(true);
                Thread.sleep(delay);
                System.out.println("open door..... " + (door.isOpen() ? "open" : "close"));
                door.setOpen(false);
                Thread.sleep(delay);
                System.out.println("close door..... " + (door.isOpen() ? "open" : "close"));
                count--;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
