package immutabletest;

import org.junit.Test;

/**
 * User: adipa
 * Date: 5/24/13
 * Time: 8:33 PM
 * Source code for functionaljava
 */
public class ImmutableTest {
    @Test
    public void testImmutable() throws InterruptedException {
        Thread threadA = new Thread(new ImmutableDoorThread(300));
        Thread threadB = new Thread(new ImmutableDoorThread(500));
        threadA.start();
        threadB.start();

        threadA.join();
        threadB.join();
    }
}

class ImmutableDoor {
    public final boolean open;

    public ImmutableDoor(boolean open) {
        this.open = open;
    }
}

class ImmutableDoorThread implements Runnable {
    private final int delay;

    ImmutableDoorThread(int delay) {
        this.delay = delay;
    }

    public void run() {
        int count = 10;
        while (count > 0) {
            try {
                final ImmutableDoor openedDoor = new ImmutableDoor(true);
                Thread.sleep(delay);
                System.out.println("open door..... " + (openedDoor.open ? "open" : "close"));
                final ImmutableDoor closedDoor = new ImmutableDoor(false);
                Thread.sleep(delay);
                System.out.println("close door..... " + (closedDoor.open ? "open" : "close"));
                count--;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

