package romannumbers;

import org.junit.Assert;
import org.junit.Test;

/**
 * User: Adipa
 * Date: 12/16/12
 * Time: 8:02 PM
 */
public class RomanNumbersSpec {
    @Test
    public void Numeric_0_ShouldReturn_Empty() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(0);
        Assert.assertEquals("", roman);
    }

    @Test
    public void Numeric_1_ShouldReturn_I() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(1);
        Assert.assertEquals("I", roman);
    }

    @Test
    public void Numeric_2_ShouldReturn_II() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(2);
        Assert.assertEquals("II", roman);
    }

    @Test
    public void Numeric_3_ShouldReturn_III() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(3);
        Assert.assertEquals("III", roman);
    }

    @Test
    public void Numeric_4_ShouldReturn_IV() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(4);
        Assert.assertEquals("IV", roman);
    }

    @Test
    public void Numeric_5_ShouldReturn_V() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(5);
        Assert.assertEquals("V", roman);
    }

    @Test
    public void Numeric_8_ShouldReturn_VIII() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(8);
        Assert.assertEquals("VIII", roman);
    }

    @Test
    public void Numeric_19_ShouldReturn_XIX() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(19);
        Assert.assertEquals("XIX", roman);
    }

    @Test
    public void Numeric_2012_ShouldReturn_MMXII() {
        RomanNumbers numbers = new RomanNumbers();
        String roman = numbers.toRoman(2012);
        Assert.assertEquals("MMXII", roman);
    }
}
