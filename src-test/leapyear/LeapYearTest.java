package leapyear;

import junit.framework.Assert;
import org.junit.Test;

/**
 * User: adipa
 * Date: 3/9/13
 * Time: 12:25 PM
 * Source code for functionaljava
 */
public class LeapYearTest {
    @Test
    public void nonLeapYear() {
        Assert.assertFalse(new Year(2013).isLeapYear());
    }

    @Test
    public void leapYear() {
        Assert.assertTrue(new Year(2012).isLeapYear());
    }

    @Test
    public void divisibleBy100NotLeapYear() {
        Assert.assertFalse(new Year(2100).isLeapYear());
    }

    @Test
    public void divisibleBy400LeapYear() {
        Assert.assertTrue(new Year(2000).isLeapYear());
    }
}
