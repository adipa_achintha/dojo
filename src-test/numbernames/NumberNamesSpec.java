package numbernames;

import org.junit.Assert;
import org.junit.Test;

/**
 * User: adipa
 * Date: 4/14/13
 * Time: 9:11 AM
 * Source code for dojo
 */
public class NumberNamesSpec {
    @Test
    public void one() {
        Assert.assertEquals("one", new NumberNames(1).toName());
    }

    @Test
    public void two() {
        Assert.assertEquals("two", new NumberNames(2).toName());
    }

    @Test
    public void twentyOne() {
        Assert.assertEquals("twenty one", new NumberNames(21).toName());
    }

    @Test
    public void thirtyFive() {
        Assert.assertEquals("thirty five", new NumberNames(35).toName());
    }

    @Test
    public void fortySeven() {
        Assert.assertEquals("forty seven", new NumberNames(47).toName());
    }

    @Test
    public void ninetyNine() {
        Assert.assertEquals("ninety nine", new NumberNames(99).toName());
    }

    @Test
    public void ninety() {
        Assert.assertEquals("ninety", new NumberNames(90).toName());
    }

    @Test
    public void oneHundredAndFiftyFive() {
        Assert.assertEquals("one hundred and fifty five", new NumberNames(155).toName());
    }

    @Test
    public void oneHundred() {
        Assert.assertEquals("one hundred", new NumberNames(100).toName());
    }

    @Test
    public void oneThousandThreeHundredAndFiftyFive() {
        Assert.assertEquals("one thousand, three hundred and thirty three", new NumberNames(1333).toName());
    }

    @Test
    public void oneThousand() {
        Assert.assertEquals("one thousand", new NumberNames(1000).toName());
    }

    @Test
    public void fifteenThousandThreeHundredAndThirtyThree() {
        Assert.assertEquals("fifteen thousand, three hundred and thirty three", new NumberNames(15333).toName());
    }

    @Test
    public void threeHundredThousandThreeHundredAndThirtyThree() {
        Assert.assertEquals("three hundred thousand, three hundred and thirty three", new NumberNames(300333).toName());
    }
}
