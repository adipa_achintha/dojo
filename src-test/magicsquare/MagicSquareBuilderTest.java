package magicsquare;

import fj.F;
import fj.F2;
import fj.Unit;
import fj.data.List;
import org.junit.Test;

/**
 * User: adipa
 * Date: 6/1/13
 * Time: 10:05 PM
 * Source code for dojo
 */
public class MagicSquareBuilderTest {
    @Test
    public void testOrder3() {
        int order = 3;
        findMagicSquares(order);
    }

    private void findMagicSquares(final int order) {
        List<SquareBuilderThread> threads = List.range(0,order).bind(List.range(0,order),new F2<Integer, Integer, SquareBuilderThread>() {
            @Override
            public SquareBuilderThread f(Integer x, Integer y) {
                Square square = new Square(List.list(new Cell(x,y,1)),order);
                return new SquareBuilderThread(square,new F<Square, Void>() {
                    @Override
                    public Void f(Square square) {
                        square.print();
                        return null;
                    }
                });
            }
        });

        threads.foreach(new F<SquareBuilderThread, Unit>() {
            @Override
            public Unit f(SquareBuilderThread thread) {
                thread.start();
                return null;
            }
        });

        threads.foreach(new F<SquareBuilderThread, Unit>() {
            @Override
            public Unit f(SquareBuilderThread thread) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
    }
}
