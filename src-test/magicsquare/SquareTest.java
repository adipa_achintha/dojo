package magicsquare;

import fj.data.List;
import junit.framework.Assert;
import org.junit.Test;

/**
 * User: adipa
 * Date: 6/1/13
 * Time: 7:29 PM
 * Source code for dojo
 */
public class SquareTest {
    @Test
    public void orderOneEmptySquare() {
        List<Cell> nil = List.nil();
        Square square = new Square(nil, 1);
        Assert.assertEquals(square.isCandidateMagicSquare(), true);
        Assert.assertEquals(square.isMagicSquare(), false);
    }

    @Test
    public void orderOneMagicSquare() {
        Square square = new Square(List.list(new Cell(0, 0, 1)), 1);
        Assert.assertEquals(square.isCandidateMagicSquare(), true);
        Assert.assertEquals(square.isMagicSquare(), true);
    }

    @Test
    public void orderTwoPossible() {
        Square square = new Square(List.list(new Cell(0, 0, 1), new Cell(0, 1, 2)), 2);
        Assert.assertEquals(square.isCandidateMagicSquare(), true);
    }

    @Test
    public void orderThreeMagic() {
        Square square = new Square(List.list(
                new Cell(0, 0, 2),
                new Cell(0, 1, 7),
                new Cell(0, 2, 6),
                new Cell(1, 0, 9),
                new Cell(1, 1, 5),
                new Cell(1, 2, 1),
                new Cell(2, 0, 4),
                new Cell(2, 1, 3),
                new Cell(2, 2, 8)), 3);
        Assert.assertEquals(square.isMagicSquare(), true);
    }

    @Test
    public void orderThreeFindNextSetWithOneMissing() {
        Square square = new Square(List.list(
                new Cell(0, 0, 2),
                new Cell(0, 1, 7),
                new Cell(0, 2, 6),
                new Cell(1, 1, 5),
                new Cell(1, 2, 1),
                new Cell(2, 0, 4),
                new Cell(2, 1, 3),
                new Cell(2, 2, 8)), 3);
        List<Square> squares = square.findNextSet();
        Assert.assertEquals(squares.length(), 1);
        Assert.assertEquals(squares.head().isMagicSquare(), true);
    }
}
