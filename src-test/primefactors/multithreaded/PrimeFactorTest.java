package primefactors.multithreaded;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * User: adipa
 * Date: 3/9/13
 * Time: 12:46 PM
 * Source code for functionaljava
 */
public class PrimeFactorTest {
    @Test
    public void primeFactorsOf1() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> l = f.primeFactors(1);

        Assert.assertTrue(l.size() == 0);
    }

    @Test
    public void primeFactorsOf2() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> l = f.primeFactors(2);

        Assert.assertTrue(l.size() == 1);
        Assert.assertTrue(count(l, 2) == 1);
    }

    @Test
    public void primeFactorsOf4() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> l = f.primeFactors(4);

        Assert.assertTrue(l.size() == 2);
        Assert.assertTrue(count(l, 2) == 2);
    }

    @Test
    public void primeFactorsOf6() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> l = f.primeFactors(6);

        Assert.assertTrue(l.size() == 2);
        Assert.assertTrue(count(l, 2) == 1);
        Assert.assertTrue(count(l, 3) == 1);
    }

    @Test
    public void primeFactorsOf8() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> l = f.primeFactors(8);

        Assert.assertTrue(l.size() == 3);
        Assert.assertTrue(count(l, 2) == 3);
    }

    @Test
    public void primeFactorsOf9() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> l = f.primeFactors(9);

        Assert.assertTrue(l.size() == 2);
        Assert.assertTrue(count(l, 3) == 2);
    }

    @Test
    public void primeFactorsOf2x2x11x31x61() {
        PrimeFactors f = new PrimeFactors();
        List<Integer> list = f.primeFactors(2 * 2 * 11 * 31 * 61);

        Assert.assertTrue(list.size() == 5);
        Assert.assertTrue(count(list, 2) == 2);
        Assert.assertTrue(count(list, 11) == 1);
        Assert.assertTrue(count(list, 31) == 1);
        Assert.assertTrue(count(list, 61) == 1);
    }

    private static Integer count(final List<Integer> list, final Integer value) {
        int count = 0;
        for (Integer integer : list) {
            if (integer.equals(value))
                count++;
        }
        return count;
    }
}

