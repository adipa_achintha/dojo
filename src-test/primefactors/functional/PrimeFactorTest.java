package primefactors.functional;

import fj.F;
import fj.data.List;
import org.junit.Assert;
import org.junit.Test;

/**
 * User: adipa
 * Date: 3/9/13
 * Time: 12:46 PM
 * Source code for functionaljava
 */
public class PrimeFactorTest {
    @Test
    public void primeFactorsOf1() {
        Assert.assertTrue(new PrimeFactors(1).primeFactors().length() == 0);
    }

    @Test
    public void primeFactorsOf2() {
        final List<Integer> list = new PrimeFactors(2).primeFactors();
        Assert.assertTrue(list.length() == 1);
        Assert.assertTrue(count(list, 2) == 1);
    }

    @Test
    public void primeFactorsOf4() {
        final List<Integer> list = new PrimeFactors(4).primeFactors();
        Assert.assertTrue(list.length() == 2);
        Assert.assertTrue(count(list, 2) == 2);
    }

    @Test
    public void primeFactorsOf6() {
        final List<Integer> list = new PrimeFactors(6).primeFactors();
        Assert.assertTrue(list.length() == 2);
        Assert.assertTrue(count(list, 2) == 1);
        Assert.assertTrue(count(list, 3) == 1);
    }

    @Test
    public void primeFactorsOf8() {
        final List<Integer> list = new PrimeFactors(8).primeFactors();
        Assert.assertTrue(list.length() == 3);
        Assert.assertTrue(count(list, 2) == 3);
    }

    @Test
    public void primeFactorsOf9() {
        final List<Integer> list = new PrimeFactors(9).primeFactors();
        Assert.assertTrue(list.length() == 2);
        Assert.assertTrue(count(list, 3) == 2);
    }

    @Test
    public void primeFactorsOf2x2x11x31x61() {
        final List<Integer> list = new PrimeFactors(2 * 2 * 11 * 31 * 61).primeFactors();
        Assert.assertTrue(list.length() == 5);
        Assert.assertTrue(count(list, 2) == 2);
        Assert.assertTrue(count(list, 11) == 1);
        Assert.assertTrue(count(list, 31) == 1);
        Assert.assertTrue(count(list, 61) == 1);
    }

    private static Integer count(final List<Integer> list, final Integer value) {
        return list.filter(new F<Integer, Boolean>() {
            @Override
            public Boolean f(Integer integer) {
                return integer.equals(value);
            }
        }).length();
    }
}

