package printdiamond;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: Adipa
 * Date: 6/3/12
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class Diamond1Spec {
    @Test
    public void Test_A() {
        char[][] expected = new char[][]{
                {'A'}};
        char[][] actual = Diamond1.createDiamond('A');

        Verify(expected, actual);
    }

    @Test
    public void Test_B() {
        char[][] expected = new char[][]{
                {' ', 'A', ' '},
                {'B', ' ', 'B'},
                {' ', 'A', ' '}};
        char[][] actual = Diamond1.createDiamond('B');

        Assert.assertThat(actual, Is.is(expected));
    }

    @Test
    public void Test_C() {
        char[][] expected = new char[][]{
                {' ', ' ', 'A', ' ', ' '},
                {' ', 'B', ' ', 'B', ' '},
                {'C', ' ', ' ', ' ', 'C'},
                {' ', 'B', ' ', 'B', ' '},
                {' ', ' ', 'A', ' ', ' '}};
        char[][] actual = Diamond1.createDiamond('C');

        Assert.assertThat(actual, Is.is(expected));
    }

    private void Verify(char[][] expected, char[][] actual) {
        Assert.assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            char[] expRow = expected[i];
            char[] actRow = actual[i];

            Assert.assertArrayEquals(expRow, actRow);
        }
    }

}
